﻿// NativeDLLCpp.cpp : 定义 DLL 应用程序的导出函数。
//

#include "stdafx.h"

#include <iostream>
#include <windows.h>

#include "CppFunction.h"

using namespace std;

#ifdef __cplusplus
#define TEXPORT extern "C" _declspec(dllexport)
#else
#define TEXPORT _declspec(dllexport)
#endif


TEXPORT int Test()
{
	DWORD start = ::GetTickCount();
	int result = 0;
	CppFunction cppFunction;
	for (int i = 0; i < 10000; i++) {
		for (int j = 0; j < 10000; j++) {
			result += cppFunction.TestFunc(i, j);
		}
	}
	DWORD end = ::GetTickCount();

	cout << "Result: " << result << " Elapsed: " << end - start << endl;

	return result;
}