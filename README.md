# CSharpVsCpp

#### 介绍
最近在看C#调用C++的相关接口问题，看到了一篇关于 [C#与C++混合编程及性能分析](https://www.cnblogs.com/zhuyingchun/p/9127068.html)
然后就有了该项目的测评，发现结果与作者的性能对比相反！ 竟然C#是C++的执行效率的将近7倍!  故将代码贴出来给大家看看，帮忙分析原因。

#### 看来我们要被常识打脸了！


#### 运行环境说明

Microsoft Visual Studio Enterprise 2017
版本 15.9.6

### 对比性能图如下：
![](https://i.loli.net/2019/01/27/5c4d173353bdf.png)

![](https://i.loli.net/2019/01/27/5c4d182986de4.png)

C#的结果：

10.Result: 1733793664 Elapsed: 942

9.Result: 1733793664 Elapsed: 884

8.Result: 1733793664 Elapsed: 850

7.Result: 1733793664 Elapsed: 892

6.Result: 1733793664 Elapsed: 858

5.Result: 1733793664 Elapsed: 892

4.Result: 1733793664 Elapsed: 883

3.Result: 1733793664 Elapsed: 917

2.Result: 1733793664 Elapsed: 835

1.Result: 1733793664 Elapsed: 877

共计：

8830

C++的结果：

10.Result: 1733793664 Elapsed: 4547

9.Result: 1733793664 Elapsed: 4750

8.Result: 1733793664 Elapsed: 4735

7.Result: 1733793664 Elapsed: 4796

6.Result: 1733793664 Elapsed: 4735

5.Result: 1733793664 Elapsed: 4813

4.Result: 1733793664 Elapsed: 4734

3.Result: 1733793664 Elapsed: 4703

2.Result: 1733793664 Elapsed: 4594

1.Result: 1733793664 Elapsed: 4563

总计：

46970


## ==============================

对比结果有点不可思议，C#比C++的执行效率还快很多，将近7倍！
## ==============================
将C++和C#的项目都切换到Release模式下编译，C++的代码速度瞬间提升上来了，而C#相对之前的Debug还略显慢个几十毫秒。
## ------------------------------
下面是对，C++代码，C#代码，C# DllImport调用C++ 代码的执行效率情况

![](https://i.loli.net/2019/01/28/5c4eca092d29e.gif)




## （其中C#直接调用托管C++混合方式不行，能编译通过，但是一运行就报错）
![](https://i.loli.net/2019/01/28/5c4eca87dd955.png)
##  搞了半天不知道为什么会报这个错?
![](https://i.loli.net/2019/01/28/5c4eca626d0d1.png)


## 扩展：.NetCore ， Node.js，Python 环境

![](https://i.loli.net/2019/01/29/5c4fc13f39dbc.png)

相同的执行代码逻辑，在保证运行结果一致的情况下，通过运行耗时看效率：C# dllImport的方式耗时最短，其次是node.js，最慢的是Python代码，无论从哪个环境下去执行都是最慢的！

![](https://i.loli.net/2019/01/29/5c501304118c6.png)

## 扩展: Java (version "1.8.0_172") 测试代码,感谢@Brave 大神贡献的代码。


Release模式跑的效果：


![](https://i.loli.net/2019/02/19/5c6b67ac95e42.png)


Debug模式下跑的效果：


![](https://i.loli.net/2019/02/19/5c6b67e0efd7d.png)

通过数据可以分析出来，Java的Release模式下执行效率和C#相当，Debug模式下比C#慢一大截。所以Java程序员提交代码尽量避免Debug模式下跑代码，至于问题的原因还有待一步进行深入研究。

本次测评围绕1个亿的数据量遍历，然后进行简单的逻辑计算，保证结果一致的情况下，通过观察时间损耗来判断语言运行的性能损耗。这是一个非常简单值观的测评模式，欢迎大家提出更多更贴切一些实际业务场景的运行逻辑模式需求进行测评！

