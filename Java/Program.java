/**
 * Created by Administrator on 2019/2/19.
 */
public class Program {
    public static void main(String[] args) {
        int t = 10;
        long total = 0;
        do
        {

            long start = System.currentTimeMillis();
            CCalc cppFunction = new CCalc();
            int result = 0;
            for (int i = 0; i < 10000; i++)
            {
                for (int j = 0; j < 10000; j++)
                {
                    result += cppFunction.TestFunc(i, j);
                }
            }
            long end = System.currentTimeMillis();

            System.out.println(t+".Result: " + result + " Elapsed: " + (end-start));
            t--;
            total += (end - start);
        } while (t > 0);
        System.out.println("共计："+total+"\r\n输入任意键退出程序.");
    }
}
