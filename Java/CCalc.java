
/**
 * Created by Administrator on 2019/2/19.
 */
public class CCalc {
    private int m_a;
    private int m_b;
    public CCalc(){}

    public CCalc(int m_a, int m_b) {
        this.m_a = m_a;
        this.m_b = m_b;
    }

    public int Calc(){
        if (m_a % 2 == 0) {
            return m_a + m_b;
        }
        if (m_b % 2 == 0) {
            return m_a - m_b;
        }
        return m_b - m_a;
    }

    public int TestFunc(int m_a, int m_b){
        CCalc calc = new CCalc(m_a, m_b);
        return calc.Calc();
    }
}
