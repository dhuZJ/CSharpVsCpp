import time
def calc(m_a, m_b):
    if m_a %2==0:
        return m_a+m_b
    if m_b%2==0:
        return m_a-m_b
    return m_b-m_a

intMax = 2147483647 + 1
t=10;
total=0;
print(int(round(time.time() * 1000)))
while t>0:
    start=(int(round(time.time() * 1000)))
    result=0
    for i in range(0,10000):
        for j in range(0,10000):
            result+=calc(i,j)
    end=(int(round(time.time() * 1000)))
    result=result%intMax
    print("%d .Result: %d Elapsed: %d" % (t,result,end-start))
    t=t-1

    total+=(end-start)

print('Total:',total)