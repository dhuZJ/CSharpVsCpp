﻿using System;

namespace CSharpDotNetCore
{
    class Program
    {
        static void Main(string[] args)
        {
            int t = 10;
            long total = 0;
            do
            {
                DateTime start = System.DateTime.Now;
                CppFunction cppFunction = new CppFunction();
                int result = 0;
                for (int i = 0; i < 10000; i++)
                {
                    for (int j = 0; j < 10000; j++)
                    {
                        result += cppFunction.TestFunc(i, j);
                    }
                }
                DateTime end = System.DateTime.Now;

                System.Console.WriteLine(t + ".Result: " + result + " Elapsed: " + (end - start).Milliseconds);
                //1733793664 -->10000 
                //2147483647	int 		int.MaxValue	
                t--;
                total += (end - start).Milliseconds;

            } while (t > 0);
            Console.WriteLine("Total:" + total + "\r\n . Press Any Key To Exit.");
            Console.ReadKey();
        }
    }
}
